﻿using UnityEngine;

namespace Events
{
    public struct GameStarted : IEvent
    {
        public readonly GameState gameState;

        public GameStarted(GameState gameState)
        {
            this.gameState = gameState;
        }
    }
}