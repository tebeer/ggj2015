﻿using UnityEngine;

namespace Events
{
    public struct CharacterSelected : IEvent
    {
        public readonly int index;

        public CharacterSelected(int index)
        {
            this.index = index;
        }
    }
}