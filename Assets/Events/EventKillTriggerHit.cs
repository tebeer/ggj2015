﻿using UnityEngine;

namespace Events
{
    public struct KillTriggerHit : IEvent
    {
        public readonly Character character;

        public KillTriggerHit(Character character)
        {
            this.character = character;
        }
    }
}