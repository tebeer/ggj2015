﻿using UnityEngine;

namespace Events
{
    public struct CharacterCreated : IEvent
    {
        public readonly Character character;

        public CharacterCreated(Character character)
        {
            this.character = character;
        }
    }
}