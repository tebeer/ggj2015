﻿using UnityEngine;

namespace Events
{
    public struct CharacterJump : IEvent
    {
        public readonly Character character;
        public readonly bool trampoline;

        public CharacterJump(Character character, bool trampoline)
        {
            this.character = character;
            this.trampoline = trampoline;
        }
    }
}