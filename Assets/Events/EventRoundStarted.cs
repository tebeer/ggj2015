﻿using UnityEngine;

namespace Events
{
    public struct RoundStarted : IEvent
    {
        public readonly Vector3 startPosition;

        public RoundStarted(Vector3 startPosition)
        {
            this.startPosition = startPosition;
        }
    }
}