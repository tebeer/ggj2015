﻿using UnityEngine;

namespace Events
{
    public struct CharacterDied : IEvent
    {
        public readonly Character character;
        public readonly GameObject gameObject;

        public CharacterDied(Character character, GameObject gameObject)
        {
            this.character = character;
            this.gameObject = gameObject;
        }
    }
}