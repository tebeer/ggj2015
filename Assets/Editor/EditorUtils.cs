﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

public static class EditorUtils
{
    private static void Select(string path)
    {
        Selection.activeObject = AssetDatabase.LoadAssetAtPath(path, typeof(ScriptableObject));
    }

	/// <summary>
	//	This makes it easy to create, name and place unique new ScriptableObject asset files.
	/// </summary>
	private static void CreateAsset<T> () where T : ScriptableObject
	{
		T asset = ScriptableObject.CreateInstance<T> ();
 
		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
		if (path == "") 
		{
			path = "Assets";
		} 
		else if (Path.GetExtension (path) != "") 
		{
			path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
		}
 
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T).ToString() + ".asset");
 
		AssetDatabase.CreateAsset (asset, assetPathAndName);

        AssetDatabase.SaveAssets ();
		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = asset;
	}

    [MenuItem("Assets/Create CharacterSettings")]
    static void CreateCharacterSettings()
    {
        EditorUtils.CreateAsset<CharacterSettings>();
    }

    [MenuItem("Assets/Create CameraSettings")]
    static void CreateCameraSettings()
    {
        EditorUtils.CreateAsset<CameraSettings>();
    }
}

#endif