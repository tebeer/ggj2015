﻿using UnityEngine;
using System.Collections;

public class CharacterHUD : MonoBehaviour
{

    public GameObject m_characterStatusPrefab;
	// Use this for initialization
	void Awake ()
    {
        for (int i = 0; i < CharacterSettings.instance.Characters.Count; i++)
        {
            CharacterDef def = CharacterSettings.instance.Characters[i];
            GameObject go = (GameObject)Instantiate(m_characterStatusPrefab);
            go.transform.SetParent(this.transform);
            go.GetComponent<CharacterStatus>().InitCharacterStatur(i, def);
        }
	
	}
	
	// Update is called once per frame
	void Update ()
    {

	}
}
