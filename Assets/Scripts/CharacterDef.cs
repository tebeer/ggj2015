﻿using UnityEngine;

public class CharacterDef : ScriptableObject
{
    public float MaxHorizontalSpeed { get { return m_maxHorizontalSpeed; } set { m_maxHorizontalSpeed = value; } }
    public float Acceleration { get { return m_acceleration; } set { m_acceleration = value; } }
    public float AirAcceleration { get { return m_airAcceleration; } set { m_airAcceleration = value; } }
    public float JumpVelocity { get { return m_jumpVelocity; } set { m_jumpVelocity = value; } }
    public GameObject Prefab { get { return m_prefab; } }
    public Color Color { get { return m_color; } }
    public AudioClip SpawnAudio { get { return m_spawnAudio; } }
    public AudioClip JumpAudio { get { return m_jumpAudio; } }

    [SerializeField]
    private float m_maxHorizontalSpeed = 5;
    [SerializeField]
    private float m_acceleration = 10;
    [SerializeField]
    private float m_airAcceleration = 5;
    [SerializeField]
    private float m_jumpVelocity = 10;
    [SerializeField]
    private GameObject m_prefab;
    [SerializeField]
    private Color m_color;
    [SerializeField]
    private AudioClip m_spawnAudio;
    [SerializeField]
    private AudioClip m_jumpAudio;
}