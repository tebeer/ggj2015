﻿using UnityEngine;
using System.Collections;
using Events;

public class CameraFollow : MonoBehaviour
{
    private Transform follow;

	private void Start ()
    {
        m_cameraSettings = CameraSettings.instance;
        Event<RoundStarted>.Register(OnRoundStarted);
        Event<CharacterCreated>.Register(OnCharacterCreated);
	}

    private void OnDestroy()
    {
        Event<RoundStarted>.Unregister(OnRoundStarted);
        Event<CharacterCreated>.Unregister(OnCharacterCreated);
    }

    private void OnRoundStarted(RoundStarted eventData)
    {
        follow = null;
        transform.position = eventData.startPosition + m_cameraSettings.Offset;
    }

	private void OnCharacterCreated(CharacterCreated eventData)
    {
        follow = eventData.character.transform;
    }
	
	private void FixedUpdate ()
    {
        if (follow && follow.gameObject.activeInHierarchy)
        {
            Vector3 position = transform.position;

            Vector3 targetPos = follow.position + m_cameraSettings.Offset;

            position = Vector3.MoveTowards(position, targetPos, m_cameraSettings.LinearSpeed * Time.deltaTime);
            position = Vector3.Lerp(position, targetPos, m_cameraSettings.RelativeSpeed * Time.deltaTime);

            transform.position = position;
        }
    }

    private CameraSettings m_cameraSettings;

}
