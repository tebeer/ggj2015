﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterMovement
{
    public void Record(Vector2 position, int frame)
    {
        if (m_startFrame == -1)
            m_startFrame = frame;

        Keyframe keyframe = new Keyframe();
        keyframe.position = position;

        if(m_keyframes == null)
            m_keyframes = new List<Keyframe>();

        m_keyframes.Add(keyframe);
    }

    public Keyframe GetKeyframe(int frame)
    {

        if(m_keyframes == null)
            return null;

        if(m_keyframes.Count == 0)
            return null;

        if (frame - m_startFrame < 0)
            return null;

        if (frame - m_startFrame >= m_keyframes.Count)
            return null;

        return m_keyframes[frame - m_startFrame];
    }

    private List<Keyframe> m_keyframes;
    private int m_startFrame = -1;

    public class Keyframe
    {
        public Vector2 position;
    }
}
