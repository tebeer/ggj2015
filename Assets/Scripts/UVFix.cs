﻿using UnityEngine;
using System.Collections;

public class UVFix : MonoBehaviour
{

    void Awake()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;

        Vector3[] vertices = mesh.vertices;
        Vector2[] uvs = mesh.uv;

        for (int i = 0; i < vertices.Length; ++i)
        {
            Vector2 uv = new Vector2();
            Vector3 worldVert = transform.localToWorldMatrix * vertices[i];

            uv.x = .2f * (worldVert.x + +worldVert.z);
            uv.y = .2f*(worldVert.y + worldVert.z);
            uvs[i] = uv;
        }
        mesh.uv = uvs;
    }

}
