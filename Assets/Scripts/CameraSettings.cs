﻿using UnityEngine;
using System.Collections;

public class CameraSettings : Settings<CameraSettings>
{

    public Vector3 Offset { get { return m_offset; } set { m_offset = value; } }
    public float LinearSpeed { get { return m_linearSpeed; } set { m_linearSpeed = value; } }
    public float RelativeSpeed { get { return m_relativeSpeed; } set { m_relativeSpeed = value; } }


    [SerializeField]
    private Vector3 m_offset;
    [SerializeField]
    private float m_linearSpeed = 1;
    [SerializeField]
    private float m_relativeSpeed = 1;
}
