﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Events;

public class Recorder : MonoBehaviour
{
	void Start ()
    {
        Event<RoundStarted>.Register(OnRoundStarted);
        Event<CharacterCreated>.Register(OnCharacterCreated);
        Event<CharacterDied>.Register(OnCharacterDied);
        Event<GameStarted>.Register(OnGameStarted);

        m_startPositions = new Dictionary<ActiveObject, Vector3>();

        foreach (ActiveObject obj in FindObjectsOfType<ActiveObject>())
        {
            m_startPositions.Add(obj, obj.transform.position);
        }

        m_oldRecords = new Dictionary<CharacterMovement, GameObject>();
	}

    void OnDestroy()
    {
        Event<RoundStarted>.Unregister(OnRoundStarted);
        Event<CharacterCreated>.Unregister(OnCharacterCreated);
        Event<CharacterDied>.Unregister(OnCharacterDied);
    }

    private void OnRoundStarted(RoundStarted eventData)
    {
        m_playFrame = 0;


        foreach (ActiveObject obj in m_startPositions.Keys)
        {
            obj.transform.position = m_startPositions[obj];
            obj.rigidbody2D.velocity = Vector2.zero;
            obj.rigidbody2D.angularVelocity = 0;
        }

    }

    private void OnCharacterCreated(CharacterCreated eventData)
    {
        if (m_currentMovement != null)
        {
            Debug.LogError("Cannot record 2 characters at the same time!");
            return;
        }

        m_recordingTransform = eventData.character.transform;
        m_currentMovement = new CharacterMovement();
    }

    private void OnGameStarted(GameStarted eventData)
    {
        m_startPositions = new Dictionary<ActiveObject, Vector3>();

        foreach (ActiveObject obj in FindObjectsOfType<ActiveObject>())
        {
            m_startPositions.Add(obj, obj.transform.position);
        }

        foreach (GameObject obj in m_oldRecords.Values)
            Destroy(obj);

        m_oldRecords = new Dictionary<CharacterMovement, GameObject>();
    }

    private void OnCharacterDied(CharacterDied eventData)
    {
        if (eventData.character == null)
            return;

        if (m_recordingTransform != eventData.character.transform)
        {
            return;
        }

        if (m_recordingTransform != null && m_currentMovement != null)
        {
            m_oldRecords.Add(m_currentMovement, m_recordingTransform.gameObject);
            m_currentMovement = null;
            m_recordingTransform = null;
        }
    }

    void FixedUpdate()
    {
        if (m_recordingTransform != null && m_currentMovement != null)
            m_currentMovement.Record(m_recordingTransform.position, m_playFrame);

        foreach (CharacterMovement movement in m_oldRecords.Keys)
        {
            CharacterMovement.Keyframe frame = movement.GetKeyframe(m_playFrame);
            CharacterMovement.Keyframe lastFrame = movement.GetKeyframe(m_playFrame - 1);
            if (frame != null)
            {
                m_oldRecords[movement].SetActive(true);
                m_oldRecords[movement].transform.position = frame.position;

                if(lastFrame != null)
                    m_oldRecords[movement].rigidbody2D.velocity = (frame.position - lastFrame.position) / Time.fixedDeltaTime;
            }
            else
            {
                if (lastFrame != null)
                {
                    Event<CharacterDied>.Send(new CharacterDied(null, m_oldRecords[movement]));
                    m_oldRecords[movement].SetActive(false);
                }
            }
        }

        m_playFrame++;
	}

    private int m_playFrame;
    private Transform m_recordingTransform;

    private CharacterMovement m_currentMovement;
    private Dictionary<CharacterMovement, GameObject> m_oldRecords;

    private Dictionary<ActiveObject, Vector3> m_startPositions;
}
