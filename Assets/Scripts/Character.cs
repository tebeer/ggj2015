﻿using UnityEngine;
using System.Collections;
using Events;

public class Character : MonoBehaviour
{
    public CharacterDef Def { get { return m_characterDef; } }

    public void Init(CharacterDef characterDef)
    {
        m_characterDef = characterDef;
        m_characterSettings = CharacterSettings.instance;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("ON TRIGGER ENTER");
        Event<KillTriggerHit>.Send(new KillTriggerHit(this));
    }

	void FixedUpdate ()
    {
        float horizontal = Input.GetAxis("Horizontal");
        bool jump = Input.GetButton("Jump");

        Vector2 position = rigidbody2D.position;
        Vector2 velocity = rigidbody2D.velocity;

        bool isOnGround = false;

        Vector2 size = GetComponent<BoxCollider2D>().size;
        size.y *= .5f;

        RaycastHit2D hit = Physics2D.BoxCast(position, size, 0, new Vector2(0, -1), size.y, m_characterSettings.WallLayerMask);

        Vector2 groundVel = Vector2.zero;

        if (hit.collider != null)
        {
            isOnGround = true;

            if (hit.rigidbody != null)
                groundVel = hit.rigidbody.velocity;
        }

        if (isOnGround)
        {
            Trampoline trampoline = hit.collider.GetComponent<Trampoline>();
            
            float jumpVel = 0;

            if (jump)
            {
                jumpVel += m_characterDef.JumpVelocity;
            }

            if (trampoline != null)
            {
                jumpVel += trampoline.jump;
                jumpVel *= trampoline.jumpMultiplier;
            }

            if (jumpVel > 0)
            {
                Event<CharacterJump>.Send(new CharacterJump(this, trampoline != null));
                velocity.y = jumpVel;
            }
            else
            {
                if (velocity.y < 0)
                    velocity.y = Mathf.Min(velocity.y, groundVel.y - 1.0f);
            }
        }

        float targetHorVel = groundVel.x;

        if (Mathf.Abs(horizontal) > 0.01f)
        {
            targetHorVel += m_characterDef.MaxHorizontalSpeed * Mathf.Sign(horizontal);
        }

        float acc = (isOnGround ? m_characterDef.Acceleration : m_characterDef.AirAcceleration);

        velocity.x = Mathf.MoveTowards(velocity.x, targetHorVel, acc * Time.deltaTime);
        velocity.x = Mathf.Lerp(velocity.x, targetHorVel, acc * Time.deltaTime);

        rigidbody2D.velocity = velocity;

    }

    private CharacterDef m_characterDef;
    private CharacterSettings m_characterSettings;
}
