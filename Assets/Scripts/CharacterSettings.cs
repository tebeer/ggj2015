﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterSettings : Settings<CharacterSettings>
{
    public LayerMask WallLayerMask { get { return m_wallLayerMask; } }
    public LayerMask KillMask { get { return m_wallLayerMask; } }
    public List<CharacterDef> Characters { get { return m_characters; } }

    [SerializeField]
    private LayerMask m_wallLayerMask = LayerMask.NameToLayer("Walls");

    [SerializeField]
    private LayerMask m_killLayerMask = LayerMask.NameToLayer("Kill");

    [SerializeField]
    private List<CharacterDef> m_characters;

}
