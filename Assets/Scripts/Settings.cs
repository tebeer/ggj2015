﻿using UnityEngine;
using System.Collections;

public class Settings<T> : ScriptableObject where T : ScriptableObject
{
    public static T instance
    {
        get
        {
            if (m_instance == null)
                m_instance = (T)Instantiate(Resources.Load<T>("Settings/" + typeof(T).ToString()));
            return m_instance;
        }
        private set
        {
        }
    }

    public static T resource
    {
        get
        {
            if (m_resource == null)
                m_resource = (T)Resources.Load<T>("Settings/" + typeof(T).ToString());
            return m_resource;
        }
        private set
        {
        }
    }

    private static T m_instance;
    private static T m_resource;
}
