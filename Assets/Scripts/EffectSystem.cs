﻿using UnityEngine;
using System.Collections;
using Events;

public class EffectSystem : MonoBehaviour
{
    public GameObject bloodParticlePrefab;

	void Start ()
    {
        Event<CharacterDied>.Register(OnCharacterDied);
	}

    void OnDestroy()
    {
        Event<CharacterDied>.Unregister(OnCharacterDied);
    }

    private void OnCharacterDied(CharacterDied eventData)
    {
        Instantiate(bloodParticlePrefab, eventData.gameObject.transform.position, Quaternion.identity);
    }

}
