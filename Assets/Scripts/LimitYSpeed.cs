﻿using UnityEngine;
using System.Collections;

public class LimitYSpeed : MonoBehaviour
{
    public float speedLimit;
    public Transform model;
    public AudioClip glideAudio;

	void Update ()
    {
        Vector2 vel = rigidbody2D.velocity;

        playTimer -= Time.deltaTime;

        if (vel.y < -speedLimit)
        {
            vel.y = -speedLimit;
            rigidbody2D.velocity = vel;
            model.localRotation = Quaternion.Slerp(model.rotation, Quaternion.Euler(90, 90, 0), Time.deltaTime * 10);

            if (playTimer <= 0)
            {
                AudioSource.PlayClipAtPoint(glideAudio, transform.position);
                playTimer = 2;
            }
        }
        else
            model.localRotation = Quaternion.Slerp(model.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * 10);

	}

    float playTimer;
}
