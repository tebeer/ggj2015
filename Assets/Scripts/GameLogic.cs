﻿using UnityEngine;
using System.Collections;
using Events;

public class GameState
{
    public bool spawnAllowed = false;
    public bool[] charactersUsed;
    public int charNumber = 0;
}

public class GameLogic : MonoBehaviour
{
    void Awake()
    {
        Events.Event<CharacterSelected>.Register(OnCharacterSelected);
        Events.Event<LevelCompleted>.Register(OnLevelCompleted);
        Events.Event<KillTriggerHit>.Register(OnKillTriggerHit);

        StartPosition startPos = FindObjectOfType<StartPosition>();
        if (!startPos)
        {
            Debug.LogError("Start position not defined!");
            return;
        }
        m_startPosition = startPos.transform.position;
    }

	void Start()
    {
        m_gameState = new GameState();
        m_gameState.spawnAllowed = true;

        m_gameState.charactersUsed = new bool[CharacterSettings.instance.Characters.Count];
        for (int i = 0; i < m_gameState.charactersUsed.Length; ++i)
            m_gameState.charactersUsed[i] = false;

        Event<GameStarted>.Send(new GameStarted(m_gameState));
	}

    void OnDestroy()
    {
        Events.Event<CharacterSelected>.Unregister(OnCharacterSelected);
        Events.Event<LevelCompleted>.Unregister(OnLevelCompleted);
    }

    private Character CreateCharacter(int id, Vector3 pos)
    {
        CharacterDef def = CharacterSettings.instance.Characters[id];

        GameObject characterObject = (GameObject)Instantiate(def.Prefab, pos, Quaternion.identity);
        Character character = characterObject.GetComponent<Character>();
        character.Init(def);

        Event<CharacterCreated>.Send(new CharacterCreated(character));

        return character;
    }

    private void OnCharacterSelected(CharacterSelected eventData)
    {
        if (m_currentCharacter != null)
        {
            m_currentCharacter.gameObject.SetActive(false);
            Event<CharacterDied>.Send(new CharacterDied(m_currentCharacter, m_currentCharacter.gameObject));

            // Destroy character component only
            Destroy(m_currentCharacter);
        }

        m_gameState.charactersUsed[eventData.index] = true;

        StartCoroutine(RestartRound(eventData.index));

    }

    private void OnLevelCompleted(LevelCompleted eventData)
    {
        if(!m_levelComplete)
            StartCoroutine(LevelCompleted());
    }

    private void OnKillTriggerHit(KillTriggerHit eventData)
    {
        if (m_currentCharacter != null)
        {
            m_currentCharacter.gameObject.SetActive(false);
            Event<CharacterDied>.Send(new CharacterDied(m_currentCharacter, m_currentCharacter.gameObject));

            // Destroy character component only
            Destroy(m_currentCharacter);
        }
    }

    public void RestartGame()
    {
        for (int i = 0; i < m_gameState.charactersUsed.Length; i++)
        {
            m_gameState.charactersUsed[i] = false;
        }
        m_gameState.charNumber = 0;
        m_gameState.spawnAllowed = true;

        if (m_currentCharacter != null)
        {
            m_currentCharacter.gameObject.SetActive(false);
            Event<CharacterDied>.Send(new CharacterDied(m_currentCharacter, m_currentCharacter.gameObject));

            // Destroy character component only
            Destroy(m_currentCharacter);
        }

        Event<GameStarted>.Send(new GameStarted(m_gameState));
    }

    private IEnumerator RestartRound(int nextCharacter)
    {
        m_gameState.spawnAllowed = false;

        Event<RoundStarted>.Send(new RoundStarted(m_startPosition));

        yield return new WaitForSeconds(.5f * m_gameState.charNumber);

        m_currentCharacter = CreateCharacter(nextCharacter, m_startPosition);

        m_gameState.charNumber++;

        m_gameState.spawnAllowed = true;

        yield break;
    }

    private IEnumerator LevelCompleted()
    {
        m_levelComplete = true;

        int nextLevel = Application.loadedLevel + 1 % Application.levelCount;

        yield return new WaitForSeconds(2.0f);

        Application.LoadLevel(nextLevel);

        yield break;
    }


    private Character m_currentCharacter;
    private Vector3 m_startPosition;
    private GameState m_gameState;
    private bool m_levelComplete;

}

