﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Events;

public class CharacterStatus : MonoBehaviour {

    private int m_characterIndex;
    private GameState m_gameState;
    private Button m_button;
    private string m_name;
    private Color m_color;

    void Awake()
    {
        Events.Event<GameStarted>.Register(OnGameStarted);
        m_button = GetComponent<Button>();
    }

    void Update()
    {
        if (m_gameState.spawnAllowed)
        {
            m_button.interactable = !m_gameState.charactersUsed[m_characterIndex];
        }
        else
        {
            m_button.interactable = false;
        }
    }

    public void InitCharacterStatur(int index, CharacterDef def)
    {
        m_characterIndex = index;
        GetComponentInChildren<Text>().text = def.name;
        GetComponent<Image>().color = def.Color;
    }

    public void SelectCharacter()
    {
        Event<CharacterSelected>.Send(new CharacterSelected(m_characterIndex));
    }

    public void OnGameStarted(GameStarted eventdata)
    {
        m_gameState = eventdata.gameState;
    }
	
    
}
