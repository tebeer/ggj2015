﻿using UnityEngine;
using System.Collections;
using Events;

public class AudioManager : MonoBehaviour
{
    public AudioClip blood;
    public AudioClip trampoline;
    public AudioClip finish;

	void Awake()
    {
        Event<CharacterCreated>.Register(OnCharacterCreated);
        Event<CharacterJump>.Register(OnCharacterJump);
        Event<CharacterDied>.Register(OnCharacterDied);
        Event<LevelCompleted>.Register(OnLevelCompleted);
	}

    void OnDestroy()
    {
        Event<CharacterCreated>.Unregister(OnCharacterCreated);
        Event<CharacterJump>.Unregister(OnCharacterJump);
        Event<CharacterDied>.Unregister(OnCharacterDied);
        Event<LevelCompleted>.Unregister(OnLevelCompleted);
    }

    private void OnCharacterJump(CharacterJump eventData)
    {
        if(eventData.trampoline)
            AudioSource.PlayClipAtPoint(trampoline, eventData.character.transform.position);
        else if(eventData.character.Def.JumpAudio != null)
            AudioSource.PlayClipAtPoint(eventData.character.Def.JumpAudio, eventData.character.transform.position);
    }

    private void OnCharacterCreated(CharacterCreated eventData)
    {
        AudioSource.PlayClipAtPoint(eventData.character.Def.SpawnAudio, eventData.character.transform.position);
    }

    private void OnCharacterDied(CharacterDied eventData)
    {
        AudioSource.PlayClipAtPoint(blood, eventData.gameObject.transform.position);
    }
    
    private void OnLevelCompleted(LevelCompleted eventData)
    {
        AudioSource.PlayClipAtPoint(finish, Vector3.zero);
    }
}
