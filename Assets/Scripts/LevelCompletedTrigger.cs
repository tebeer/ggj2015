﻿using UnityEngine;
using System.Collections;
using Events;

public class LevelCompletedTrigger : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.layer == LayerMask.NameToLayer("Finish"))
        {
            Event<LevelCompleted>.Send(new LevelCompleted());
            Destroy(this);
        }
    }
}
