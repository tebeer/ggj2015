﻿namespace Events
{
    public interface IEvent {}

    public static class Event<T> where T : struct, IEvent
    {
        public static void Send(T parameters)
        {
            if(m_event != null)
                m_event(parameters);
        }

        public static void Register(EventDelegate function)
        {
            m_event += function;
        }

        public static void Unregister(EventDelegate function)
        {
            m_event -= function;
        }

        public delegate void EventDelegate(T parameters);
        private static event EventDelegate m_event;
    }
}