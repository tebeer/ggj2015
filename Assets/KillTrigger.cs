﻿using UnityEngine;
using System.Collections;
using Events;

public class KillTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("ON TRIGGER ENTER");
        Event<KillTriggerHit>.Send(new KillTriggerHit(other.gameObject.GetComponent<Character>()));
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
